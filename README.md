# Проект ИВС
Здесь будут храниться файлы, связаннные с проектом по измерительным видеоинформационным системам

# Требования
- ОЭПиС (желательно измерительная)
- наличие обработки сигналов/изображений
- структурная схема
- выбор и обоснование элементой базы (габаритно-энергетический расчёт)
- оптическая часть
- экономическая оценка затрат на проектирование и производство
- необходимый состав команды для разработки (какие должности, сколько человек)
- возможные риски проекта, пути их минимизации

# Ограничения
- срок сдачи - 13 мая
- до 20:00 МСК 12 мая необходимо отправить пояснительную записку и презентацию на защиту
- максимальная стоимость проекта - 10 млн. рублей
- в защите участвует вся команда